# Lost in Translation

Lost in Translation is a web service for translation words and text into American Sign Language.

## Background

TODO

## Deployment

TODO

## Usage

Log in to access the translation service.

Click "Home" to navigate to the translation page. Write some text and either click the Translate button or hit enter to translate.

Click "Profile" to navigate to the profile page. Displays your last ten translations and contains buttons to both log out and clear your translation history.